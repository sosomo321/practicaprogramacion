package clases;


public class Empleado {

	private String nombre;
	private int edad;
	private double sueldo;
	private String puesto;
	private String DNI;
	public Empleado(String nombre, int edad, double sueldo, String puesto, String DNI) {
		this.nombre = nombre;
		this.edad = edad;
		this.sueldo = sueldo;
		this.puesto = puesto;
		this.DNI = DNI;
	}
	public Empleado(String nombre) {
		this.nombre=nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String DNI) {
		this.DNI = DNI;
	}

	@Override
	public String toString() {
		return "Empleado\nnombre: " + nombre + ", edad: " + edad + ", sueldo: " + sueldo + ", puesto: " + puesto+ ", DNI: "+DNI;
	}
	
}
