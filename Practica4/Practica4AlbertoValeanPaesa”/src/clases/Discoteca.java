package clases;

import java.util.Arrays;
import java.util.Scanner;

public class Discoteca {
	public static Scanner input = new Scanner(System.in);
	private String nombre;
	private String direccion;
	private int aforo;
	private double precioEntradaGenerica;
	private int numEmpleados;
	Empleado[] empleados;
	int contadorEmpleados=0;
	/**
	 * @Override
	 * @param nombre
	 * @param direccion
	 * @param aforo
	 * @param precioEntradaGenerica
	 * @param numEmpleados
	 */

	public Discoteca(String nombre, String direccion, int aforo, double precioEntradaGenerica, int numEmpleados) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.aforo = aforo;
		this.precioEntradaGenerica = precioEntradaGenerica;
		this.empleados = new Empleado[numEmpleados];
	}

	public Discoteca() {

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getAforo() {
		return aforo;
	}

	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

	public double getPrecioEntradaGenerica() {
		return precioEntradaGenerica;
	}

	public void setPrecioEntrada(double precioEntradaGenerica) {
		this.precioEntradaGenerica = precioEntradaGenerica;
	}

	public int getNumEmpleados() {
		return numEmpleados;
	}

	public void setNumEmpleados(int numEmpleados) {
		this.numEmpleados = numEmpleados;
	}
	
	
	
	public String toString() {
		return "Discoteca \nnombre=" + nombre + ", direccion=" + direccion + ", aforo=" + aforo
				+ ", precioEntradaGenerica=" + precioEntradaGenerica + ", numEmpleados=" + numEmpleados + ", empleados="
				+ Arrays.toString(empleados) + ", contadorEmpleados=" + contadorEmpleados ;
	}

	/**
	 * 
	 * return discoteca, devuelve objeto discoteca
	 */
	public static Discoteca crearDiscoteca() {
		System.out.println("ESTE APLICACI�N TE PERMITE GUARDAR DATOS DE TU DISCOTECA \n                    Y SUS EMPLEADOS");
		System.out.println();
		System.out.println("�Como se llama tu discoteca?");
		String nombre = input.nextLine();
		System.out.println("�Cual es la direcci�n?");
		String direccion = input.nextLine();
		System.out.println("�Cuanto aforo tiene?");
		int aforo = input.nextInt();
		System.out.println("�Cual es el precio de la entrada?");
		double precioEntradaGenerica = input.nextDouble();
		System.out.println("�Que licencia deseas adquirir?(Capacidad maxima de empleados)");
		System.out.println("5, 10, 25, 50 empleados");
		int numEmpleados = input.nextInt();
		Discoteca discoteca = new Discoteca(nombre, direccion, aforo, precioEntradaGenerica, numEmpleados);
		return discoteca;
	}
	/**
	 * 
	 * @param nombre del empleado que se desea dar de alta
	 * @param edad del empleado que se desea dar de alta
	 * @param sueldo del empleado que se desea dar de alta
	 * @param puesto del empleado que se desea dar de alta
	 * @param DNI del empleado que se desea dar de alta
	 */
	
	public void altaEmpleados(String nombre, int edad, double sueldo, String puesto, String DNI) {
			for (int i = 0; i < empleados.length; i++) {
				if (empleados[i] == null) {
					empleados[i] = new Empleado(nombre);
					empleados[i].setEdad(edad);
					empleados[i].setSueldo(sueldo);
					empleados[i].setPuesto(puesto);
					empleados[i].setDNI(DNI);
					System.out.println("Empleado " + empleados[i].getNombre() + " dado de alta de forma satisfactoria");
					break;
				}
			}
	}
	/**
	 * 
	 * @param nombre del empleado que se desea buscar
	 */
	public void mostrarEmpleadoNombre(String nombre) {
		int contadorEmpleados = 0;
		for (int i = 0; i < empleados.length; i++) {
			if (empleados[i] != null) {
				if (empleados[i].getNombre().equals(nombre)) {
					System.out.println(empleados[i]);
					contadorEmpleados++;
				}
			}
		}
		if (contadorEmpleados == 0) {
			System.out.println("ningun empleado con ese nombre");
		}
	}

	/**
	 * 
	 * @param DNI del empleado que se desea buscar
	 */
	public void buscarEmpleadoDNI(String DNI) {
		int contadorDNI = 0;
		for (int i = 0; i < empleados.length; i++) {
			if (empleados[i] != null) {
				if (empleados[i].getDNI().equals(DNI)) {
					System.out.println(empleados[i]);
					contadorDNI++;
				}
			}
		}
		if (contadorDNI == 0) {
			System.out.println("Ningun empleado encontrado con ese DNI");
		}
	}

	/**
	 * 
	 * @param DNI del empleado que se desea borrar
	 */
	public void eliminarEmpleado(String DNI) {
		int contadorDNI = 0;
		for (int i = 0; i < empleados.length; i++) {
			if (empleados[i] != null) {
				if (empleados[i].getDNI().equals(DNI)) {
					empleados[i] = null;
					contadorDNI++;
				}
			}
		}
		if (contadorDNI == 0) {
			System.out.println("Ningun empleado encontrado con ese DNI");
		}

	}


	public void mostrarTodos() {
		for (int i = 0; i < empleados.length; i++) {
			if (empleados[i] != null) {
				System.out.println(empleados[i]);
			}
		}
	}

	/**
	 * 
	 * @param nombreAntiguo   nombre por el que buscas el empleado para cambiar los datos
	 * @param nombreNuevo   nuevo nombre que deseas ponerle a dicho empleado
	 * @param edad   nueva edad que deseas ponerle
	 * @param sueldo   nuevo sueldo que deseas ponerle
	 * @param puesto   nuevo puesto que deseas ponerle
	 * @param DNI   nuevo DNI que deseas ponerle
	 */
	public void reemplazarEmpleado(String nombreAntiguo, String nombreNuevo, int edad, double sueldo, String puesto,
			String DNI) {
		int contador = 0;
		for (int i = 0; i < empleados.length; i++) {
			if (empleados[i] != null) {
				if (empleados[i].getNombre().equals(nombreAntiguo)) {
					empleados[i].setNombre(nombreNuevo);
					empleados[i].setEdad(edad);
					empleados[i].setSueldo(sueldo);
					empleados[i].setPuesto(puesto);
					empleados[i].setDNI(DNI);
					contador++;
				}
			}
		}
		if (contador == 0) {
			System.out.println("Ningun empleado con ese nombre");
		}
	}
	/**
	 * 
	 * @param nombre   nombre por el que buscas el empleado
	 * @param sueldo   el sueldo que deseas actulizar
	 */
	// CAMBIAR EL SUELDO DE UN EMPLEADO
	public void cambiarSueldoEmpleado(String nombre, double sueldo) {
		for (int i = 0; i < empleados.length; i++) {
			if (empleados[i] != null) {
				if (empleados[i].getNombre().equals(nombre)) {
					empleados[i].setSueldo(sueldo);
					System.out.println("Su nuevo sueldo es " + empleados[i].getSueldo() + "�");
				}
			}
		}
	}
	
	/**
	 * 
	 * @param puesto por el que deseas filtrar a los empleados
	 */
	// MOSTRAR POR PUESTO
	public void filtrarPorPuesto(String puesto) {
		int contador = 0;
		for (int i = 0; i < empleados.length; i++) {
			if (empleados[i] != null) {
				if (empleados[i].getPuesto().equals(puesto)) {
					System.out.println(empleados[i].getNombre() + "  ");
					contador++;
				}
			}
			if (contador == 0) {
				System.out.println("No hay ningun empleado con ese puesto");
			}
		}
	}
}
