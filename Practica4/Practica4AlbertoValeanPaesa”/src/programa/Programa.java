package programa;

import java.util.Scanner;

import clases.Discoteca;

public class Programa {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		// variables de los empleados
		String nombre;
		int edad;
		double sueldo;
		String puesto;
		String DNI;
		int contadorEmpleados = 0;
		// declarar objeto discoteca1
		Discoteca discoteca1 = new clases.Discoteca();

		// crear objeto discoteca1
		discoteca1 = clases.Discoteca.crearDiscoteca();

		// menu con los diferentes metodos
		int opcion;
		do {
			System.out.println(" ");
			System.out.println("*******************************************************************");
			System.out.println("1.- Dar de alta un empleado");
			System.out.println("2.- Mostrar Datos de un empleado introduciendo el nombre");
			System.out.println("3.- Mostrar empleado introduciendo el DNI");
			System.out.println("4.- Eliminar empleado introduciendi el DNI");
			System.out.println("5.- Mostrar todos los empleados");
			System.out.println("6.- Cambiar el sueldo de un empleado");
			System.out.println("7.- Mostrar los empleados por su puesto");
			System.out.println("8.- Cambiar los datos de un empleado");
			System.out.println("9.- Salir");
			System.out.println();
			System.out.println("ELIGE UNA DE LAS OPCIONES");
			System.out.println("*******************************************************************");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				if (contadorEmpleados <= discoteca1.getNumEmpleados()) {
					System.out.println("Has elegido la opcion de dar de alta un empleado");
					System.out.println("nombre del empleado");
					nombre = input.nextLine();
					System.out.println("Edad de " + nombre);
					edad = input.nextInt();
					System.out.println("Sueldo de " + nombre);
					sueldo = input.nextDouble();
					input.nextLine();
					System.out.println("Puesto de " + nombre);
					System.out.println("Elige uno entre los siguientes: camarero, RRPP o seguridad");
					puesto = input.nextLine();
					System.out.println("DNI de " + nombre);
					DNI = input.nextLine();
					discoteca1.altaEmpleados(nombre, edad, sueldo, puesto, DNI);
					contadorEmpleados++;
				} else {
					System.out.println("cupo maximo alcanzado");
				}
				break;
			case 2:
				System.out.println("Has elegido la opcion de consultar un empleado");
				System.out.println("Dame su nombre");
				nombre = input.nextLine();
				discoteca1.mostrarEmpleadoNombre(nombre);
				break;
			case 3:
				System.out.println("Has elegido la opcion de consultar un empleado");
				System.out.println("Dame su DNI");
				DNI = input.nextLine();
				discoteca1.buscarEmpleadoDNI(DNI);
				break;
			case 4:
				System.out.println("Has elegido la opcion de eliminar un empleado");
				System.out.println("Dame su DNI");
				DNI = input.nextLine();
				discoteca1.eliminarEmpleado(DNI);
				break;
			case 5:
				System.out.println("Aqui puedes ver todos los empleados de tu discoteca");
				discoteca1.mostrarTodos();
				break;
			case 6:
				System.out.println("Has elegido la opcion de cambiar sueldo de un empleado");
				System.out.println("Dame el nombre del afortunado");
				nombre = input.nextLine();
				System.out.println("Dime el nuevo sueldo");
				sueldo = input.nextDouble();
				discoteca1.cambiarSueldoEmpleado(nombre, sueldo);
				input.nextLine();
				break;
			case 7:
				System.out.println("Has elegido la opcion de buscar empleados por su puesto de trabajo");
				System.out.println("Elige entre uno de los puestos()");
				System.out.println("camarero, RRPP o seguridad");
				puesto = input.nextLine();
				discoteca1.filtrarPorPuesto(puesto);
				break;
			case 8:
				String nombreNuevo;
				System.out.println("Has eledigo la opcion de cambiar los datos de un empleado");
				System.out.println("Dame el nombre del empleado que deseas modificar");
				nombre = input.nextLine();
				System.out.println("Dame el nuevo nombre");
				nombreNuevo = input.nextLine();
				System.out.println("Dame el nuevo DNI");
				DNI = input.nextLine();
				System.out.println("Dame el nuevo puesto entre camarero, RRPP o seguridad");
				puesto = input.nextLine();
				System.out.println("Dame el nuevo sueldo");
				sueldo = input.nextDouble();
				System.out.println("Dame la nueva edad");
				edad = input.nextInt();
				discoteca1.reemplazarEmpleado(nombre, nombreNuevo, edad, sueldo, puesto, DNI);
				break;
			default:
				if (opcion != 9) {
					System.out.println("opcion no contemplada");
				}
				break;
			}
		} while (opcion != 9);
		input.close();
	}

}
